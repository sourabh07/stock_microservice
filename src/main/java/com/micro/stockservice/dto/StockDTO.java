package com.micro.stockservice.dto;

import java.math.BigDecimal;
import java.util.Date;

public class StockDTO {
	
	private String stockName;
	private BigDecimal lastPrice;
	private BigDecimal lastChange;
	private BigDecimal latPeg;
	private BigDecimal lastDividend;
	private Date lastUpdated;

	
	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public BigDecimal getLastPrice() {
		return lastPrice;
	}

	public void setLastPrice(BigDecimal lastPrice) {
		this.lastPrice = lastPrice;
	}

	public BigDecimal getLastChange() {
		return lastChange;
	}

	public void setLastChange(BigDecimal lastChange) {
		this.lastChange = lastChange;
	}

	public BigDecimal getLatPeg() {
		return latPeg;
	}

	public void setLatPeg(BigDecimal latPeg) {
		this.latPeg = latPeg;
	}

	public BigDecimal getLastDividend() {
		return lastDividend;
	}

	public void setLastDividend(BigDecimal lastDividend) {
		this.lastDividend = lastDividend;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	@Override
	public String toString() {
		return "StockEntity [stockName=" + stockName + ", lastPrice=" + lastPrice + ", lastChange="
				+ lastChange + ", latPeg=" + latPeg + ", lastDividend=" + lastDividend + ", lastUpdated=" + lastUpdated
				+ "]";
	}
}
