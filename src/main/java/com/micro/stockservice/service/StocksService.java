package com.micro.stockservice.service;

import com.micro.stockservice.dto.StockDTO;

public interface StocksService {
	
	StockDTO getStockQuote(String stockName);
	
}
