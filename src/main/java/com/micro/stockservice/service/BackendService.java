package com.micro.stockservice.service;

import java.util.List;

import com.micro.stockservice.dto.StockDTO;

public interface BackendService {
	
	List<StockDTO> getUserStocksFromDB(String userName);
	
}
