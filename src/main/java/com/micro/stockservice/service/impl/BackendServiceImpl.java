package com.micro.stockservice.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.micro.stockservice.dto.StockDTO;
import com.micro.stockservice.service.BackendService;

@Service
public class BackendServiceImpl implements BackendService {

	@Autowired
	RestTemplate restTemplate;
	
	
	
	@Override
	public List<StockDTO> getUserStocksFromDB(String userName) {
		
		ResponseEntity<List<StockDTO>> stockResponse = restTemplate
				.exchange("http://database/back/stock/"+userName,HttpMethod.GET,null, new ParameterizedTypeReference<List<StockDTO>>() {});
		List<StockDTO> stocks = stockResponse.getBody();
		System.out.println(stocks);
		/*System.out.println(updateUserStocksInDB(userName,stocks));*/
		return stocks;
	}
	
	/*
	private Boolean updateUserStocksInDB(String userName, List<StockDTO> stocks) {
		
		UserDetailsDTO user = new UserDetailsDTO();
		user.setFirstName(userName);
		user.setStocks(stocks);
		RequestEntity<UserDetailsDTO> request;
		ResponseEntity<Boolean> response = null;
		try {
			request = RequestEntity
				     .post(new URI("http://database/back/putStock/"+userName))
				     .accept(MediaType.APPLICATION_JSON)
				     .body(user);
			response = restTemplate.exchange(request, Boolean.class);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return response.getBody();
	}
*/	
	
	
}
