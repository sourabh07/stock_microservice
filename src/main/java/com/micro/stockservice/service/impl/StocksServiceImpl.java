package com.micro.stockservice.service.impl;

import java.io.IOException;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.micro.stockservice.dto.StockDTO;
import com.micro.stockservice.service.StocksService;

import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.quotes.stock.StockQuote;

@Service
public class StocksServiceImpl implements StocksService {

	@Override
	public StockDTO getStockQuote(String stockName) {
		
		StockDTO dto = new StockDTO();
		try {
			Stock stock = YahooFinance.get(stockName);
			StockQuote quote = stock.getQuote();
			dto.setLastChange(quote.getChange());
			dto.setLastDividend(stock.getDividend().getAnnualYield());
			dto.setLastPrice(quote.getPrice());
			dto.setLastUpdated(new Date());
			dto.setLatPeg(quote.getAsk());
			dto.setStockName(stockName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dto;
	}

}
