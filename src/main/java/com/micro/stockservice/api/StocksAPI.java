package com.micro.stockservice.api;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.micro.stockservice.dto.StockDTO;
import com.micro.stockservice.service.BackendService;
import com.micro.stockservice.service.StocksService;

@RestController
@RequestMapping("/stock")
public class StocksAPI {
	
	@Autowired
	StocksService stockService;

	@Autowired
	BackendService backendService;

	@RequestMapping(value = "/{userName}", method = RequestMethod.GET)
	public List<StockDTO> getStocksForUser(@PathVariable("userName") final String userName)
	{
		List<StockDTO> stocks = backendService.getUserStocksFromDB(userName);
		List<StockDTO> stocksResponse = new ArrayList<StockDTO>();
		for (StockDTO stockDTO : stocks) {
			stocksResponse.add(stockService.getStockQuote(stockDTO.getStockName()));
		}
		return stocksResponse;
	}
}
